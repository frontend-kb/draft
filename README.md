## General

- https://kentcdodds.com/blog/aha-programming

- [Axel's post](https://medium.com/@rauschma/note-that-default-exporting-objects-is-usually-an-anti-pattern-if-you-want-to-export-the-cf674423ac38) is in conflict with [ES6 Module Gotchas](https://geedew.com/es6-module-gotchas/)

- https://egghead.io/lessons/javascript-implement-inversion-of-control (see all)

## Testing

https://kentcdodds.com/blog/avoid-nesting-when-youre-testing

## React

https://kentcdodds.com/blog/usememo-and-usecallback

https://kentcdodds.com/blog/should-i-usestate-or-usereducer

https://www.youtube.com/watch?v=WV0UUcSPk-0